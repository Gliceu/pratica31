
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Gliceu Camargo <gliceu@utfpr.edu.br>
 */
public class Pratica31 {
    public static void main(String[] args) {
       Date inicio = new Date();
       String meuNome = "Gliceu Fernandes camargo"; 
        System.out.println(meuNome.toUpperCase());//imprime o nome completo em maísculo.
          System.out.printf( "\""+meuNome.toUpperCase().substring(17, 18)+""+ meuNome.toLowerCase().substring(18, 24)+", "
           +meuNome.toUpperCase().substring(0,1)+". "+ meuNome.toUpperCase().substring(7,8)+"."+"\""+"\n" );
        
        Calendar dataNascimento = new GregorianCalendar(1965,Calendar.MARCH, 02); 
         Calendar dataAtual = new GregorianCalendar();
          long a= (dataAtual.getTime()).getTime()-(dataNascimento.getTime()).getTime();
            System.out.println(a/(1000*60*60*24));
             Date fim = new Date();
              System.out.println(fim.getTime() - inicio.getTime());
    }
}
